### classes[3] = "Containers 101"

#### Laboratório de Bioinformática 2024-2025

![Logo EST](assets/logo-ESTB.png)


© Francisco Pina Martins 2018-2024

---

### What is a container?

* &shy;<!-- .element: class="fragment" -->Virtual environment on a host machine
    * &shy;<!-- .element: class="fragment" -->Isolated
        * &shy;<!-- .element: class="fragment" -->Container cannot intrude on the host
        * &shy;<!-- .element: class="fragment" -->Host cannot intrude on the container
    * &shy;<!-- .element: class="fragment" -->Has its own set of system resources
    * &shy;<!-- .element: class="fragment" -->Has it's own set of subordinate processes

<img src="assets/Containers_101.png" class="fragment" style="background:none; border:none; box-shadow:none">

---

### Vm vs container

* &shy;<!-- .element: class="fragment" -->VMs use static resource allocation
* &shy;<!-- .element: class="fragment" -->Containers use dynamic resource allocation

<img src="assets/Container_vs_VM.png" class="fragment" style="background:none; border:none; box-shadow:none">

|||

### Containers are...

<div style="float: left; width:25%">

</br>
</br>
</br>

* &shy;<!-- .element: class="fragment" -->Lightweight
* &shy;<!-- .element: class="fragment" -->Flexible
* &shy;<!-- .element: class="fragment" -->Portable
* &shy;<!-- .element: class="fragment" -->Scalable

</div>
<div style="float: right; width:75%" class="fragment">

![Lots of containers](assets/containers.jpg)

</div>

---

### Docker

* &shy;<!-- .element: class="fragment" -->Is a container manager
    * &shy;<!-- .element: class="fragment" -->Starts & stops containers
    * &shy;<!-- .element: class="fragment" -->Runs processes on containers
    * &shy;<!-- .element: class="fragment" -->Creates container filesystems - images

<img src="assets/blueprint.png" class="fragment" style="background:none; border:none; box-shadow:none">

|||

### Images Vs. Containers

* &shy;<!-- .element: class="fragment" -->A docker image is like a class in programming
    * &shy;<!-- .element: class="fragment" -->Inert
    * &shy;<!-- .element: class="fragment" -->Immutable
    * &shy;<!-- .element: class="fragment" -->Created from a YML `Dockerfile` & `build` command
    * &shy;<!-- .element: class="fragment" -->Strated with a `run` command

|||

### Images Vs. Containers

* &shy;<!-- .element: class="fragment" -->A docker container is like a class' instance
    * &shy;<!-- .element: class="fragment" -->A running process
    * &shy;<!-- .element: class="fragment" -->In constant change
    * &shy;<!-- .element: class="fragment" -->Created from a `run` command
    * &shy;<!-- .element: class="fragment" -->Ephemeral

---

### Demo time

* Install Docker
* Docker-build
* View images
* View containers
* Docker run
* Mount volumes

---

### Installing Docker

* &shy;<!-- .element: class="fragment" -->[Instructions for *buntu](https://itsfoss.com/install-docker-ubuntu/)
* &shy;<!-- .element: class="fragment" -->[Instructions for other distros](https://docs.docker.com/engine/install/#supported-platforms)
* &shy;<!-- .element: class="fragment" -->Take 5' to install it

<br>
</br>

&shy;<!-- .element: class="fragment" -->![Take 5](assets/clock-5.svg)

* &shy;<!-- .element: class="fragment" -->If you don't want to prefix every command with `sudo` run this command and reboot your OS:

<div class="fragment">

``` bash
sudo usermod -aG docker $USER
```

</div>

---

### Building an image

* &shy;<!-- .element: class="fragment" -->Create a new directory and get the [Example Dockerfile](assets/Dockerfile) into it
* &shy;<!-- .element: class="fragment" -->Look at its contents before running the next commands

<div class="fragment">

``` bash
docker build -t mafft_image ./  # Build the image

docker image list  # Lists all local images
```

</div>

---

### Run a container

* &shy;<!-- .element: class="fragment" -->Containers are based on images, and get spawned with the `docker run` command

<div class="fragment">

``` bash
docker run --name "my_mafft_container_1" -i -t mafft_image /bin/bash
# --name "my_mafft_container": name your CONTAINER
# -i: interactive
# -t: alocate a pseudo-terminal
# mafft_image: image name
# /bin/bash: the command that gets run
```

</div>

* &shy;<!-- .element: class="fragment" -->Be careful as argument order matters in the `docker run` command!

---

### Using the container

* &shy;<!-- .element: class="fragment" -->Next we are going to use the container's software stack to get a FASTA file and align it

<div class="fragment">

``` bash
mkdir /data
ids=$(wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/\?db\=nucleotide\&term\="Psammodromus algirus[organism], cytb[gene], not genome"\&retmax=100 -O - |grep -i "^<Id>" | sed 's/[^0-9]//g' | tr "\n" "," |rev |cut -c 2- |rev)  # Get the list of IDs, comma separated
wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/\?db\=nucleotide\&id\=$ids\&rettype\=fasta -O /data/P_algirus_cytb.fasta
mafft /data/P_algirus_cytb.fasta > /data/P_algirus_cytb_aln.fasta
exit
```

</div>

* &shy;<!-- .element: class="fragment" -->This is fun, but it is also of limited use
    * &shy;<!-- .element: class="fragment" -->How do I get files in and out of a container?

---

### Volumes

* &shy;<!-- .element: class="fragment" -->The simplest way to deal with file I/O between host and container systems is the use of volumes
* &shy;<!-- .element: class="fragment" -->A volume represents a path that is shared between both systems
    * &shy;<!-- .element: class="fragment" -->Uses the syntax `-v /full/path/to/host/directory:/full/path/to/container/directory`

<div class="fragment">

``` bash
mkdir ~/container_io
docker run --name "my_mafft_container_2" -v /home/$USER/container_io:/data -i -t mafft_image /bin/bash
```
</div>

* &shy;<!-- .element: class="fragment" -->Now, try getting the aligned sequences again

<div class="fragment">

``` bash
ids=$(wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/\?db\=nucleotide\&term\="Psammodromus algirus[organism], cytb[gene], not genome"\&retmax=100 -O - |grep -i "^<Id>" | sed 's/[^0-9]//g' | tr "\n" "," |rev |cut -c 2- |rev)  # Get the list of IDs, comma separated
wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/\?db\=nucleotide\&id\=$ids\&rettype\=fasta -O /data/P_algirus_cytb.fasta
mafft /data/P_algirus_cytb.fasta > /data/P_algirus_cytb_aln.fasta
exit
```

</div>

* &shy;<!-- .element: class="fragment" -->Finally check `~/container_io` **in the host** system!

---

### Container management

* &shy;<!-- .element: class="fragment" -->"Finished" containers will remain reachable
    * &shy;<!-- .element: class="fragment" -->Can be seen with the `docker ps` command
    * &shy;<!-- .element: class="fragment" -->Can be deleted with the `docker rm` command
    * &shy;<!-- .element: class="fragment" -->Can be accessed with the `docker exec` command

<div class="fragment">

``` bash
docker ps --all  # View all containers, including stopped ones
docker rm my_mafft_container_1  # Get rid of the first container
docker start my_mafft_container_2  # Resume the container
docker exec -i -t my_mafft_container_2 /bin/bash  # Run a new process on the container
```

</div>

---

### Making it public

* &shy;<!-- .element: class="fragment" -->Once you have an image ready for whatever you need it, you can publish it
* &shy;<!-- .element: class="fragment" -->The main repository is [docker hub](https://hub.docker.com)
* &shy;<!-- .element: class="fragment" -->To be published images need a "special" name:
    * &shy;<!-- .element: class="fragment" -->`username/image_name:TAG`
        * &shy;<!-- .element: class="fragment" -->`TAG` is akin to a version number

&shy;<!-- .element: class="fragment" -->![Docker hub icon](assets/docker-hub.png)

---

### References

* [An intro to containers](https://www.developer.com/design/containers-101.html)
* [VMs vs containers](https://www.backblaze.com/blog/vm-vs-containers/)
* [Images vs containers](https://stackoverflow.com/a/26960888/3091595)
* [Installing docker](https://itsfoss.com/install-docker-ubuntu/)
* [Getting started with Docker](https://docs.docker.com/get-started/)
* [Docker training](https://training.play-with-docker.com/)
